"""
This script exports detection/ description using pretrained model.

Author: You-Yi Jau, Rui Zhu
Date: 2019/12/12
"""

## basic
import argparse
import time
import csv
import yaml
import os
import logging
from pathlib import Path

import numpy as np
from imageio import imread
from tqdm import tqdm
# from tensorboardX import SummaryWriter

## torch
import torch
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torch.optim
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data

## other functions
from utils.utils import (
    tensor2array,
    save_checkpoint,
    load_checkpoint,
    save_path_formatter,
)
from utils.utils import getWriterPath
from utils.loader import dataLoader, modelLoader, pretrainedLoader
from utils.utils import inv_warp_image_batch
from models.model_wrap import SuperPointFrontend_torch, PointTracker

## parameters
from settings import EXPER_PATH
from datasets.videobox_dataset import *

#### util functions

def cogent_peugeot_dataset(max_images_per_stream = 2000, shuffle=False):
    videos_calibrations = [
        ('datasets/cogent_peugeot/2021-09-17_22_38_18.vbox3', 'datasets/cogent_peugeot/2021-09-30.yaml'),
        ('datasets/cogent_peugeot/2021-09-18_17_23_21.vbox3', 'datasets/cogent_peugeot/2021-09-30.yaml'),
        ('datasets/cogent_peugeot/2021-09-27_21_29_15.vbox3', 'datasets/cogent_peugeot/2021-09-30.yaml'),
        ('datasets/cogent_peugeot/2021-10-24_15_59_49.vbox3', 'datasets/cogent_peugeot/2021-10-29.yaml'),
        ('datasets/cogent_peugeot/2021-10-24_16_22_38.vbox3', 'datasets/cogent_peugeot/2021-10-29.yaml')]
    dataset =  create_multivideo_dataset(videos_calibrations, max_images = max_images_per_stream, shuffle=shuffle, seed = 834659834)
    return dataset

#### end util functions

def get_pts_desc_from_agent(val_agent, img, device="cpu"):
    """
    pts: list [numpy (3, N)]
    desc: list [numpy (256, N)]
    """
    pts, pts_desc, dense_desc, heatmap = val_agent.run(img, onlyHeatmap=False, train=False)
    outs = {"pts": pts[0], "desc": pts_desc[0]}
    return outs


def transpose_np_dict(outs):
    for entry in list(outs):
        outs[entry] = outs[entry].transpose()

def infer(config, output_dir, top_k=100):
    from utils.loader import get_save_path
    from utils.var_dim import squeezeToNumpy

    # basic settings
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    logging.info("train on device: %s", device)
    with open(os.path.join(output_dir, "config.yml"), "w") as f:
        yaml.dump(config, f, default_flow_style=False)
    
    #create dir to store predictions
    save_path = get_save_path(output_dir)
    save_output = save_path / "../predictions"
    os.makedirs(save_output, exist_ok=True)

    #create cg dataset
    dataset = cogent_peugeot_dataset(max_images_per_stream=None, shuffle=False)

    # model loading
    from utils.loader import get_module
    # Val_model_heatmap = get_module("", config["front_end_model"])
    path = config["pretrained"]
    nms_dist = config["model"]["nms"]
    conf_thresh = config["model"]["detection_threshold"]
    nn_thresh = config["model"]["nn_thresh"]

    fe = SuperPointFrontend_torch(
            config=config,
            weights_path=path,
            nms_dist=nms_dist,
            conf_thresh=conf_thresh,
            nn_thresh=nn_thresh,
            cuda=False,
            device=device,
        )
    # fe.net_parallel()

    ## tracker
    tracker = PointTracker(max_length=2, nn_thresh=fe.nn_thresh)

    import cv2
    for i in tqdm(range(0, dataset.npairs, 30)):
        right, left, meta = dataset.get_pair(i, "")
        right = np.array(right)
        left = np.array(left)
        right_orig = right.copy()
        left_orig = left.copy()

        right = right.astype('float32') / 255.0
        left = left.astype('float32') / 255.0

        right = torch.tensor(right, dtype=torch.float32).permute((2,0,1)).unsqueeze(0)
        left = torch.tensor(left, dtype=torch.float32).permute((2,0,1)).unsqueeze(0)

        outs = get_pts_desc_from_agent(fe, right, device=device)
        pts, desc = outs["pts"], outs["desc"]  # pts: np [3, N]
        tracker.update(pts, desc)

        outs = get_pts_desc_from_agent(fe, left, device=device)
        pts, desc = outs["pts"], outs["desc"]  # pts: np [3, N]
        tracker.update(pts, desc)

        #transpose to [pair, (x,y,conf)] and get top_k
        matches = tracker.get_matches().transpose()
        scores = tracker.get_mscores().transpose()

        matches = matches[scores[:, 2].argsort()]
        # matches = matches[scores[:, 2].argsort()[::-1]]
        matches = matches[:top_k,:]
        
        total_img = np.vstack((right_orig,left_orig))
        matches[:,3] += left_orig.shape[0]
        line_thickness = 1
        for k in range(matches.shape[0]):
            color = list(np.random.random(size=3) * 256)
            cv2.line(total_img, (int(matches[k,0]), int(matches[k,1])), (int(matches[k,2]), int(matches[k,3])), color, thickness=line_thickness)
        # print('detection/' + str(i) + '.png')
        # cv2.imwrite('detection/' + str(i) + '.png', total_img)
        cv2.imshow('r', total_img)
        cv2.waitKey(-1)

        tracker.clear_desc()


if __name__ == "__main__":
    # global var
    torch.set_default_tensor_type(torch.FloatTensor)
    logging.basicConfig(
        format="[%(asctime)s %(levelname)s] %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.INFO,
    )

    # add parser
    parser = argparse.ArgumentParser()
    parser.add_argument("config", type=str)
    parser.add_argument("exper_name", type=str)
    parser.add_argument("--top_k", type=int, default=200)
    parser.add_argument("--save", type=bool, default=False)

    args = parser.parse_args()
    with open(args.config, "r") as f:
        config = yaml.load(f)
    print("check config!! ", config)

    output_dir = os.path.join(EXPER_PATH, args.exper_name)
    os.makedirs(output_dir, exist_ok=True)

    # with capture_outputs(os.path.join(output_dir, 'log')):
    # logging.info("Running command {}".format(args.command.upper()))
    # args.func(config, output_dir, args)
    infer(config, output_dir, args.top_k)
