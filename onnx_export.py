"""
This script exports detection/ description using pretrained model.

Author: You-Yi Jau, Rui Zhu
Date: 2019/12/12
"""

## basic
import argparse
import time
import csv
import yaml
import os
import logging
from pathlib import Path

import numpy as np
from imageio import imread
from tqdm import tqdm
# from tensorboardX import SummaryWriter

## torch
import torch
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torch.optim
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data

## other functions
from utils.utils import (
    tensor2array,
    save_checkpoint,
    load_checkpoint,
    save_path_formatter,
)
from utils.utils import getWriterPath
from utils.loader import dataLoader, modelLoader, pretrainedLoader
from utils.utils import inv_warp_image_batch
from models.model_wrap import SuperPointFrontend_torch, PointTracker

## parameters
from settings import EXPER_PATH
from datasets.videobox_dataset import *


def export(config, output_name, width, height, batch):
    from utils.loader import get_save_path
    from utils.var_dim import squeezeToNumpy

    # basic settings
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    logging.info("ONNX export on device: %s", device)
    # with open(os.path.join(output_dir, "config.yml"), "w") as f:
    #     yaml.dump(config, f, default_flow_style=False)
    

    # model loading
    from utils.loader import get_module
    path = config["pretrained"]
    nms_dist = config["model"]["nms"]
    conf_thresh = config["model"]["detection_threshold"]
    nn_thresh = config["model"]["nn_thresh"]

    fe = SuperPointFrontend_torch(
            config=config,
            weights_path=path,
            nms_dist=nms_dist,
            conf_thresh=conf_thresh,
            nn_thresh=nn_thresh,
            cuda=False,
            device=device,
        )
    model = fe.get_model()
    model.eval()
    model.export = True

    dummy_input = torch.rand((batch, 3, height, width))

    input_names = ["image"]
    output_names = ["heatmap", "desc"]
    torch.onnx.export(model, dummy_input.to(device), output_name, verbose=False, input_names=input_names,
        output_names=output_names, opset_version=9, keep_initializers_as_inputs = False)






if __name__ == "__main__":
    # global var
    torch.set_default_tensor_type(torch.FloatTensor)
    logging.basicConfig(
        format="[%(asctime)s %(levelname)s] %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.INFO,
    )

    # add parser
    parser = argparse.ArgumentParser()
    parser.add_argument("config", type=str)
    parser.add_argument("--output_name", type=str)
    parser.add_argument("--width",  type=int, default=640)
    parser.add_argument("--height",  type=int, default=480)
    parser.add_argument("--batch",  type=int, default=1)

    args = parser.parse_args()
    with open(args.config, "r") as f:
        config = yaml.load(f)
    print("check config!! ", config)

    export(config, args.output_name, args.width, args.height, args.batch)
