If it's only needed to export/infer then skip PREPARE/TRAIN stages and follow instructions right from EXPORT/INFER.


PREPARE
1. Download cogent dataset from cnn:/home/ceadmin/dkorchemkin/r2d2/data/cogent_peugeot
2. Copy cogent_peugeot folder into ./datasets folder
3. Generate downsampled cylindrical frames for train and test:
	python3 gen_c_dataset.py datasets/cyl_dataset
4. Generate pseudo ground truth for cyl_dataset train split:
	python3 export.py export_detector_homoAdapt configs/cogent/magicpoint_coco_export20_train.yaml cyl_dataset
5. Generate pseudo ground truth for cyl_dataset val split:
	python3 export.py export_detector_homoAdapt configs/cogent/magicpoint_coco_export20_val.yaml cyl_dataset
6. Copy /logs/cyl_dataset/predictions folder into datasets/cyl_dataset
7. Download COCO 2014 train images from https://cocodataset.org/#download
8. Extract images into datasets/cyl_dataset/train
9. Download COCO pseudo ground truth from https://drive.google.com/drive/folders/1nnn0UbNMFF45nov90PJNnubDyinm2f26?usp=sharing
10. Extract COCO labels into datasets/cyl_dataset/predictions/train

OR download pre-generated dataset from cnn:/home/ceadmin/dkorchemkin/r2d2/data/cyl_dataset.zip and extract inside datasets/ folder

TRAIN
Train medium model (3.6 GMAC):
	python3 train4.py train_joint configs/cogent/csp_medium.yaml csp_medium_relu6_0 --eval
	
Train small model (1.96 GMAC):
	python3 train4.py train_joint configs/cogent/csp_small.yaml csp_small_relu6_0 --eval
	
EXPORT
Export medium model:
	python3 onnx_export.py configs/cogent/csp_medium_export.yaml --output medium.onnx --width 680 --height 256 --batch 1
Export small model:
	python3 onnx_export.py configs/cogent/csp_small_export.yaml --output small.onnx --width 680 --height 256 --batch 1
	
INFER
1. Download cogent dataset from cnn:/home/ceadmin/dkorchemkin/r2d2/data/cogent_peugeot
2. Copy cogent_peugeot folder into ./datasets folder
3. Infer small:
	python3 infer.py configs/cogent/csp_small_export.yaml out --top_k 300
4. Infer medium:
	python3 infer.py configs/cogent/csp_medium_export.yaml out --top_k 300
