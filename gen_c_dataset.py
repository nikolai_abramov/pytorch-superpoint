import cv2
import os
import argparse
from tqdm import tqdm

from datasets.videobox_dataset import *

def cogent_peugeot_dataset(max_images_per_stream = 2000, shuffle=False):
    videos_calibrations = [
        ('datasets/cogent_peugeot/2021-09-17_22_38_18.vbox3', 'datasets/cogent_peugeot/2021-09-30.yaml'),
        ('datasets/cogent_peugeot/2021-09-18_17_23_21.vbox3', 'datasets/cogent_peugeot/2021-09-30.yaml'),
        ('datasets/cogent_peugeot/2021-09-27_21_29_15.vbox3', 'datasets/cogent_peugeot/2021-09-30.yaml'),
        ('datasets/cogent_peugeot/2021-10-24_15_59_49.vbox3', 'datasets/cogent_peugeot/2021-10-29.yaml'),
        ('datasets/cogent_peugeot/2021-10-24_16_22_38.vbox3', 'datasets/cogent_peugeot/2021-10-29.yaml')]
    dataset =  create_multivideo_dataset(videos_calibrations, max_images = max_images_per_stream, shuffle=shuffle)
    print(dataset)
    return dataset

NUM_TILES_Y = 1
NUM_TILES_X = 2
TRG_W = 680
TRG_H = 256
TRAIN_RATIO = 7
FRAME_STEP = 10
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate Cylindrical dataset')
    parser.add_argument( "--out", default="", type=str, metavar="FILE", help="output path")
    
    args = parser.parse_args()
    os.makedirs(args.out, exist_ok=True)

    splits_path = []
    #train path
    train_path = os.path.join(args.out, 'train')
    splits_path.append(train_path)
    os.makedirs(train_path, exist_ok=True)
    #dev path
    dev_path = os.path.join(args.out, 'val')
    splits_path.append(dev_path)
    os.makedirs(dev_path, exist_ok=True)
    np.random.seed(0)

    dataset = cogent_peugeot_dataset(max_images_per_stream=None, shuffle=False)

    for i in tqdm(range(0, dataset.npairs, FRAME_STEP)):
        right, left, meta = dataset.get_pair(i, "")
        right = np.array(right)
        right = cv2.cvtColor(right, cv2.COLOR_RGB2BGR)

        dim = (TRG_W, TRG_H)
        right = cv2.resize(right, dim, interpolation = cv2.INTER_AREA)      

        #split into tiles
        tiles = []
        x_step = right.shape[1] // NUM_TILES_X
        y_step = right.shape[0] // NUM_TILES_Y
        for x in range(NUM_TILES_X):
            for y in range(NUM_TILES_Y):
                split = right[y*y_step:(y+1)*y_step, x*x_step:(x+1)*x_step]
                tiles.append(split)

        for n, s in enumerate(tiles):
            output_filename = str(i) + "_" + str(n) + ".png"

            split_index = np.random.randint(10) // TRAIN_RATIO
            output_path = os.path.join(splits_path[split_index], output_filename)
            cv2.imwrite(output_path, s)
            # input(output_path)
        # cv2.imshow("Right", np.array(right))
        # cv2.waitKey()