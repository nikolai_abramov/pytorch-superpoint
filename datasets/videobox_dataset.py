import cv2
import py_dataset_tools as dt
import numpy as np
from PIL import Image
import pyFishAccels as fa
from copy import copy

import imgaug as ia
import imgaug.augmenters as iaa

class Dataset(object):
    ''' Base class for a dataset. To be overloaded.
    '''
    root = ''
    img_dir = ''
    nimg = 0

    def __len__(self):
        return self.nimg

    def get_key(self, img_idx):
        raise NotImplementedError()

    def get_filename(self, img_idx, root=None):
        return os.path.join(root or self.root, self.img_dir, self.get_key(img_idx))

    def get_image(self, img_idx):
        from PIL import Image
        fname = self.get_filename(img_idx)
        try:
            return Image.open(fname).convert('RGB')
        except Exception as e:
            raise IOError("Could not load image %s (reason: %s)" % (fname, str(e)))

    def __repr__(self):
        res =  'Dataset: %s\n' % self.__class__.__name__
        res += '  %d images' % self.nimg
        res += '\n  root: %s...\n' % self.root
        return res
        
class CatPairDataset ():
    ''' Concatenation of several pair datasets.
    '''
    def __init__(self, *datasets):
        # CatDataset.__init__(self, *datasets)
        self.datasets = datasets
        pair_offsets = [0]
        for db in datasets:
            pair_offsets.append(db.npairs)
        self.pair_offsets = np.cumsum(pair_offsets)
        self.npairs = self.pair_offsets[-1]

    def __len__(self):
        return self.npairs

    def __repr__(self):
        fmt_str = "CatPairDataset("
        for db in self.datasets:
            fmt_str += str(db).replace("\n"," ") + ', '
        return fmt_str[:-2] + ')'

    def pair_which(self, i):
        pos = np.searchsorted(self.pair_offsets, i, side='right')-1
        assert pos < self.npairs, 'Bad pair index %d >= %d' % (i, self.npairs)
        return pos, i - self.pair_offsets[pos]

    def pair_call(self, func, i, *args, **kwargs):
        b, j = self.pair_which(i)
        return getattr(self.datasets[b], func)(j, *args, **kwargs)

    def get_pair(self, i, output=()):
        b, i = self.pair_which(i)
        return self.datasets[b].get_pair(i, output)

    def get_flow_filename(self, pair_idx, *args, **kwargs):
        return self.pair_call('get_flow_filename', pair_idx, *args, **kwargs)

    def get_mask_filename(self, pair_idx, *args, **kwargs):
        return self.pair_call('get_mask_filename', pair_idx, *args, **kwargs)

    def get_corres_filename(self, pair_idx, *args, **kwargs):
        return self.pair_call('get_corres_filename', pair_idx, *args, **kwargs)


class VideoBoxDataset(Dataset):
    peugeot_cameras = {'Right camera':'46-de-7a-a4-81-20', 'Left camera':'49-08-b5-c3-e2-2f', 'Front camera':'1d-9a-27-24-bc-10', 'Rear camera':'88-70-2e-ac-63-c2'}
    def __init__(self, frames_provider, camera, max_images = None, shuffle = True, otp_ids = peugeot_cameras, seed = 31415):
        Dataset.__init__(self)

        self.reader = frames_provider
        ids = self.reader.getIds()[0]
        otp_id = ids.index(otp_ids[camera])

        self.index = otp_id
        self.shuffle = shuffle

        self.nimg = self.reader.getFramesCount()
        self.frame_ids = np.arange(self.nimg)
        if max_images is not None:
            self.nimg = min(max_images, self.nimg)

        self.camera = camera
        self.otp_id = otp_id

        if shuffle:
            rng = np.random.default_rng(seed)
            rng.shuffle(self.frame_ids)

    def convert_image(image, fmt):
        dict = {dt.CE_ALG_FMT.CE_FMT_NV12: cv2.COLOR_YUV2RGB_NV12,
                dt.CE_ALG_FMT.CE_FMT_I420: cv2.COLOR_YUV2RGB_I420, }
        if fmt in dict.keys():
            return cv2.cvtColor(image, dict[fmt])
        if fmt == dt.CE_ALG_FMT.CE_FMT_BAYER:
            image16bit = np.frombuffer(image.tobytes(),
                                    dtype=np.uint16).reshape(image.shape[0], image.shape[1]//2)
            min, max = np.min(image16bit[16:-16, :]), np.max(image16bit[16:-16, :])
            image32bit = np.float32(255.)*(image16bit - min)/np.float32(max-min)
            return cv2.cvtColor(image32bit.astype(np.uint8), cv2.COLOR_GRAY2BGR)
        return cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

    def get_image(self, img_idx):
        assert img_idx < self.nimg
        if self.shuffle:
            img_idx = self.frame_ids[img_idx]
        self.reader.seekIndex(img_idx)

        result = self.reader.nextResult()

        image = result.getImages()[self.index]
        format = result.getFmts()[self.index]

        img = VideoBoxDataset.convert_image(image, format)
        return img

    def __repr__(self):
        s = 'VideoBoxDataset\n'
        s += '\n\tCamera: ' + self.camera
        s += '\n\tOTP-id: ' + self.otp_id
        s += '\n\tImages: {}'.format(self.nimg)
        return s

class CylinderPairDataset():
    CYL_WIDTH = 1024
    CYL_HEIGHT = 768
    CYL_SKIP = 384
    CYL_HFOV = 180
    CYL_RATIO = .2
    CYL_ASPECT = 1.
    ANGLE_RANGE = 15.

    def __init__(self, dataset, camera, camera_system, seed = 31415):
        # CylinderPairDataset.__init__()
        self.dataset = dataset
        self.npairs = dataset.nimg
        self.nimg = dataset.nimg
        self.get_image = dataset.get_image
        self.get_key = dataset.get_key
        self.get_filename = dataset.get_filename
        self.root = None

        system = fa.CameraSystem(camera_system)
        self.camera = system.getParamByName(camera)
        self.rng = np.random.default_rng(seed)

        #augments
        self.seq = iaa.Sequential([
            iaa.Sometimes(0.25, iaa.AdditiveGaussianNoise(scale=(0, 0.05*255))),
            iaa.Sometimes(0.5, iaa.MultiplyHueAndSaturation((0.8, 1.2), per_channel=True)),
            iaa.Sometimes(0.5, iaa.JpegCompression(compression=(0, 50))),
            iaa.Sometimes(0.2, iaa.imgcorruptlike.Fog(severity=1)),
            iaa.Sometimes(0.2, iaa.Rain(speed=(0.1, 0.3), drop_size=(0.10, 0.20)))
        ])
        ia.seed(seed)

    def  get_pair(self, i, output):
        if isinstance(output, str):
            output = output.split()

        img_fisheye = self.dataset.get_image(i)

        remap_left, R_left, model_left = self.gen_new_view()
        remap_right, R_right, model_right = self.gen_new_view()

        R_r2l = R_left @ R_right.T

        skip_rows = CylinderPairDataset.CYL_SKIP
        img_left = cv2.remap(img_fisheye, remap_left, None, cv2.INTER_CUBIC)[:-skip_rows,:,:]
        img_right = cv2.remap(img_fisheye, remap_right, None, cv2.INTER_CUBIC)[:-skip_rows,:,:]

        H,W = img_left.shape[0:2]
        xy = np.mgrid[0:H, 0:W][::-1]
        pts_right = xy.reshape((2, H*W)).T

        h0 = 1e3
        rays_right = model_right.Image2World(pts_right, h0)
        rays_right[:,2] -= h0
        rays_left = rays_right @ R_r2l.T
        rays_left[:,2] += h0
        pts_left = model_left.World2Image(rays_left, h0, True)

        flow_abs = pts_left.reshape((img_left.shape[0], img_right.shape[1], 2))

        meta = {}
        if 'flow' in output or 'aflow' in output:
            meta['aflow'] = flow_abs
            meta['flow'] = flow_abs - xy.transpose((1,2,0))

        img_left = np.asarray(Image.fromarray(img_left))
        img_right = np.asarray(Image.fromarray(img_right))

        #extra aug
        right_aug = self.seq(image=img_right)
        left_aug = self.seq(image=img_left)
        # right_show = cv2.cvtColor(right_aug, cv2.COLOR_RGB2BGR)
        # left_show = cv2.cvtColor(left_aug, cv2.COLOR_RGB2BGR)
        # cv2.imshow('right', right_show)
        # cv2.imshow('left', left_show)
        # cv2.waitKey(-1)

        return right_aug, left_aug, meta #need camera name, camera *.yaml, fisheye

    def gen_new_view(self):
        phi = CylinderPairDataset.ANGLE_RANGE
        dyaw, dpitch, droll = self.rng.uniform(-phi, phi, 3)

        cam = copy(self.camera)
        cam.Extrinsics.yawAngle += dyaw
        cam.Extrinsics.pitchAngle += dpitch
        cam.Extrinsics.rollAngle += droll

        R = fa.rotationMatrixFromAngles(cam.Extrinsics.yawAngle, cam.Extrinsics.pitchAngle, cam.Extrinsics.rollAngle)

        yaw_center = -round(cam.Extrinsics.yawAngle / 90) * 90
        view = fa.CylView()
        view.imWidth = CylinderPairDataset.CYL_WIDTH
        view.imHeight = CylinderPairDataset.CYL_HEIGHT
        view.centerYawDegree = yaw_center
        view.hFovDegree = CylinderPairDataset.CYL_HFOV
        view.horRatio = CylinderPairDataset.CYL_RATIO
        view.aspectCompression = CylinderPairDataset.CYL_ASPECT

        remap = fa.MakeCylindrMesh(params = cam, view = view)
        return (remap, R, view)

def create_cylinder_dataset(frames_provider, camera_system, camera, max_images = None, shuffle = True, seed = 31415):
    return CylinderPairDataset(
                VideoBoxDataset(frames_provider,
                                camera = camera,
                                max_images = max_images,
                                shuffle = shuffle,
                                seed = seed),
                camera = camera,
                camera_system = camera_system,
                seed = seed)

def create_multicam_dataset(video, camera_system, max_images = None, shuffle = True, seed = 31415):
    rng = np.random.default_rng(seed)
    frames_provider = dt.CreateProvider(video)

    cameras = ['Front camera', 'Right camera', 'Rear camera', 'Left camera']
    datasets = [create_cylinder_dataset(frames_provider, camera_system, c, max_images,
                                        shuffle, seed = rng.integers(1,2**63)) for c in cameras]
    return CatPairDataset(*datasets)

def create_multivideo_dataset(videos_calibrations, max_images = None, shuffle = True, seed = 31415):
    dataset =  CatPairDataset(*[create_multicam_dataset(v, c, max_images, shuffle, seed)
        for v,c in videos_calibrations])
    # input(dataset)
    return dataset
